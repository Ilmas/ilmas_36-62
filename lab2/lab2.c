
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <mcheck.h>

#define N 10

char** inp_str(int count,int maxlen){
	char buffer[N];
	sprintf (buffer, "%%%ds", maxlen);
	char **str_array;  
	str_array = (char **)malloc(sizeof(char *)*count);
    for (int i = 0; i < count ; i++){
        scanf("%s", buffer); 
        str_array[i] = (char *)malloc(sizeof(char)*strlen(buffer)+1); 
        strcpy(str_array[i], buffer);                                 
    }
    return str_array; 
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]);
    }
    free(mas); 
}

int str_len(char* string){
	return strlen(string);
	}
void out_str(char* string, int length, int number){
	printf("Номер строки: %d Строка: %s Длина строки: %d \n", number, string, length);
}

int main(){
	printf("Вариант 2 \n Расположить строки по убыванию длины \n Входные параметры:\n Выходные параметры:\n 1.Количество перестановок\n 2.Длина большей строки\n\n\n");
	mtrace();
	int count = 0;
	int MAXLEN = 0;
	int p=0;
	printf("Введите кол-во строк:");
	scanf("%d", &count);
	printf("Введите максимальную длину:");
	scanf("%d", &MAXLEN);
	char **str_array = NULL; 

	str_array = inp_str(count, MAXLEN);
	printf("\n\nНеотсортированный массив\n");
	for (int x=0; x<count; x++){
		int n=x;
		int len = str_len(str_array[x]);
		out_str(str_array[x], len, n);}
		
	for (int i=0;i<count-1;i++){
  	for (int k=0;k<count-1;k++){
		if(strlen(str_array[k])<strlen(str_array[k+1])){
		char* t;    
			t = str_array[k];
			str_array[k] = str_array[k+1];     
			str_array[k+1] = t;             
			p++;	
			}
		}
	}	
	printf("\n\n\nОтсортированный массив\nКоличество перестановок:%d\n", p);
	for (int x=0; x<count; x++){
		int n=x;
		int len = str_len(str_array[x]);
		out_str(str_array[x], len, n);}
		
	freeMas(str_array, count);	
	return 0;
}
