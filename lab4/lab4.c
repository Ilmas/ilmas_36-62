

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int count(char* string){
	int k;
	int len=strlen(string);
    for(int i=0; i<len; i++)
        {
            if((string[i]>='0' && string[i]<='9'))
            k++;
        }
        return k;
	}

int main(int argc,char *argv[]){
  int len=atoi(argv[1]);
  char c;
  char string[256];
  FILE * in = fopen("test.txt","r");
  FILE * out = fopen("test2.txt","w");
  if (in == NULL)
    {
        fprintf(stderr,"Error open file\n");
        return 1;
    }
   while ( (c=fgetc(in) ) != EOF)
    {
        fgets(string, sizeof(string), in);
   		if (len==count(string)) fputs (string, out);
    }
  fclose(in);
  fclose(out);
}
