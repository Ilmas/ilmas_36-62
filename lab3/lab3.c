

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <mcheck.h>

struct sportsmen{
		char name[50];
		char sport[50];
		int medals;
	};

void printsportsmen(struct sportsmen *sp){
		printf("Фамилия студента %s \n", sp->name);
		printf("Вид спорта %s \n", sp->sport);
		printf("Кол-во медалей %d \n", sp->medals);
}
void inp_sportsmen(struct sportsmen *sp){
    printf("Введите Фамилию:");
    scanf("%s", sp->name);
    printf("Введите вид спорта:");
    scanf("%s", sp->sport);
    printf("Введите кол-во медалей:");
    scanf("%d", &sp->medals);
}

int cmp(const void *p1, const void *p2){
    struct sportsmen * sp1 = *(struct sportsmen**)p1;
    struct sportsmen * sp2 = *(struct sportsmen**)p2;
    return strcmp(sp1->name,sp2->name);
}

int main(){
	mtrace();
	printf("Вариант 9\n");
	int count = 0;
	printf("Введите кол-во спортсменов:");
    scanf("%d", &count);
    struct sportsmen** sp = (struct sportsmen**)malloc(sizeof(struct sportsmen**)*count);
    for (int i = 0; i < count ; i++){
        sp[i] = (struct sportsmen*) malloc (sizeof(struct sportsmen));
        inp_sportsmen(sp[i]);
    } 
    printf("\n\n");
    qsort(sp, count, sizeof(struct sportsmen*),cmp);
    for (int i = 0; i < count; i++)
    {
		if (sp[i]->medals > 0) printsportsmen(sp[i]);
    }
    
    for (int i = 0; i < count; i++)
    {
        free(sp[i]);
    }
    free(sp);
	return 0;
}
